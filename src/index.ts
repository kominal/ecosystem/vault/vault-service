import Service from '@kominal/service-util/helper/service';

export const service = new Service({
	id: 'vault-service',
	name: 'Vault Service',
	description: 'Manages passwords.',
	jsonLimit: '16mb',
	routes: [],
	database: 'vault-service',
});
service.start();
